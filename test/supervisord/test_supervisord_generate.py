#!/usr/bin/env python3

import configparser
import os
import subprocess
import unittest

from felix_test_case import FelixTestCase


class TestSupervisordGenerate(FelixTestCase):

    def test_supervisord_generate(self):

        testdir = os.path.join('test', 'supervisord')
        config = os.path.join(testdir, 'felix-tbed.cfg')
        template = os.path.join(testdir, 'supervisord.conf.jinja')

        try:
            subprocess.check_output(' '.join(("./felix-supervisord-generate",
                                              '--template', template,
                                              '--destination', '.',
                                              config)),
                                    stderr=subprocess.STDOUT, shell=True, encoding="UTF-8")

            config_files = [
                'supervisord_pc-tbed-felix-00.cern.ch.conf',
                'supervisord_pc-tbed-felix-04.cern.ch.conf',
                'supervisord_pc-tbed-felix-06.cern.ch.conf',
                'supervisord_pc-tbed-felix-07.cern.ch.conf'
            ]
            for c in config_files:
                print("Processing ", c)
                self.assertTrue(os.path.isfile(c))

                parser = configparser.ConfigParser(inline_comment_prefixes=(';',))
                parser.read(c)

                reference = configparser.ConfigParser(inline_comment_prefixes=(';',))
                reference.read(os.path.join(testdir, c))

                cmd_diff = "diff " + os.path.join(os.getcwd(), c) + " " + os.path.join(os.getcwd(), testdir, c)
                try:
                    subprocess.check_output(cmd_diff, shell=True)
                except subprocess.CalledProcessError as ex:
                    print(ex.output.decode('utf-8'))

                self.assertTrue(parser._sections == reference._sections, parser._sections)

        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            self.assertEqual(e.returncode, 0)


if __name__ == '__main__':
    unittest.main()
