#include <iostream>
#include <string>
#include <chrono>
#include <thread>
#include <sys/types.h>
#include <unistd.h>

#include "docopt/docopt.h"


static const char USAGE[] =
R"(felix-sv-test - Exits with the desired return code after a certain time.

    Usage:
      felix-sv-test [options] <code>

    Options:
      -h --help                   Show this screen.
      --name=<name>               String to label the output [default: sv-test]
      --time=<N>                  Return after after N seconds [default: 1]
      --fault=<fault>             segfault, divzero [default: None]
)";


int main(int argc, char **argv)
{
    std::map<std::string, docopt::value> args
        = docopt::docopt(USAGE,
                         { argv + 1, argv + argc },
                         true,                    // show help if requested
                         std::string(argv[0]));  // version string

   int code = args["<code>"].asLong();
   int time = args["--time"].asLong();
   std::string name = args["--name"].asString();
   std::string exception = args["--fault"].asString();
   pid_t pid = getpid();

   std::chrono::seconds span(time);
   std::this_thread::sleep_for(span);

   if(exception == "segfault"){
    int* ptr = nullptr;
    std::cout << "Causing segfault..." << std::endl;
    std::cout << "Accessing nullptr " << ptr[0] << std::endl;
   }

   if(exception == "divzero"){
    std::cout << "Causing floating point exception..." << std::endl;
    int zero = 0;
    int infinite = 1/zero;
    std::cout << infinite << std::endl;
   }

   std::cout << name << " (" << pid << "): returning code " << code << std::endl;
   if ( code ) {
       std::cerr << name << " (" << pid << "): returning error code " << code << std::endl;
   }
   return code;
}
