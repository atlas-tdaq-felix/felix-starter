# felix-starter
this repository contains a collections of tools to configure and control Supervisor.
No application is called felix-starter.


## felix-supervisord-generate
felix-supervisord-generate is the tool to generate Supervisor config files.
Two inputs are needed:

1. a host description file like such as [felix-tbed.cfg](etc/felix-tbed.cfg)
2. a Jinja template such as [supervisor.conf.jinga](etc/supervisord.conf.jinja)

A number of command line options allow to specify settings:
```
Usage:
    felix-supervisord-generate [options] <felix-config-file>

Options:
    -t, --template <supervisord-template>   Supervisord template [default: supervisord.conf.jinja]
    -d, --destination DIRECTORY             Directory for the generates SUpervisor config file [default: /det/tdaq/felix-operation/supervisord/]
    -c, --config-dir DIRECTORY              Directory with the configuration files (elinkconfig) [default: /det/tdaq/felix-operation/config]
    -b, --bus-dir DIRECTORY                 Directory for the bus [default: /det/tdaq/felix-operation/bus]
    -m, --tmp-dir DIRECTORY                 Absolute directory for the temporary files of supervisord [default: /dev/shm]
    -g, --bus-groupname GROUP_NAME          Group name to use for the bus [default: FELIX]
    -l, --log-dir DIRECTORY                 Directory for the logfiles [default: /logs/felix-operation]
    -u, --user USER                         User of supervisor HTTP interface [default: username]
    -p, --password PWD                      Password of supervisor HTTP interface [default: password]
    -q, --tdaq RELEASE                      TDAQ release for felix2atlas and felix-supervisor [default: tdaq-11-02-00].
    --tdaq-env                              Generates TDAQ environment for felix-supervisor
    --error-out FIFO                        FIFO for errors [default: /dev/shm/felix-errors.json]
    --stats-out FIFO                        FIFO for stats [default: /dev/shm/felix-statistics.json]
    --error-port PORT                       PORT for errors [default: 53401]
    --stats-port PORT                       PORT for stats [default: 53400]
    --dcs-watermark SIZE                    DCS NetIO watermark in Bytes [default: 972]
    --dcs-unbuffered                        Use unbuffered DCS socket
    --error-elink ELINK                     ELINK for errors [default: 0x1100]
    --stats-elink ELINK                     ELINK for stats [default: 0x1000]
    -v, --verbose                           Verbose output

Arguments:
    <felix-config-file>         FELIX configuration file
```

## felix-supervisor
felix-supervisor is the application in charge of starting applications in sequence.
felix-supervisor is also registered as eventlistener and reports to ERS. 

On each FELIX PC
1. the Supervisor daemon is started at boot time by Systemd ([unit](systemd/felix-daq.service)).
2. Supervisor executes [felix-supervisor](supervisor/felix_supervisor.py), registered as Event Listener
3. felix-supervisor commands the execution of processes via Supervisor's RCP interface.
    - initilisation and configuration processes are started synchronously
    - DAQ applications are started asynchronously
    - felix-supervisor remains alive as passive listener
4. During the whole time logging messages are forwarded to ERS.

The execution sequence of managed applications is expected to be encoded in the group names according to the format `<stage number>_<name>_<blocking>`.

## felix-daq.service
The systemd unit of felix-daq.service can be found [here](systemd/felix-daq.service).

## Utilities
The repository includes 
- `test/test-app.cpp`, a C++ application that sleeps for a configurable amount of time and returns the desired return code.
