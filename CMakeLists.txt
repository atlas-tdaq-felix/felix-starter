cmake_minimum_required(VERSION 3.5)

set(PACKAGE felix-starter)
set(PACKAGE_VERSION 0.0.1)

include(FELIX)

# from externals
felix_add_external(docopt ${docopt_version} ${BINARY_TAG})

set(CMAKE_CXX_STANDARD 17)

# Exec
#
tdaq_add_executable(felix-sv-test test/test-app.cpp)
target_link_libraries(felix-sv-test docopt)

# Tests
add_test(felix-starter-supervisord-generate test/supervisord/test_supervisord_generate.py)

# Extras
add_custom_target(felix-starter-python ALL
	COMMAND cmake -E copy_directory ${CMAKE_CURRENT_SOURCE_DIR}/python ${CMAKE_CURRENT_BINARY_DIR})
add_custom_target(felix-starter-supervisor ALL
    COMMAND cmake -E copy_directory ${CMAKE_CURRENT_SOURCE_DIR}/supervisor ${CMAKE_CURRENT_BINARY_DIR})
add_custom_target(felix-starter-tests ALL
    COMMAND cmake -E copy_directory ${CMAKE_CURRENT_SOURCE_DIR}/test ${CMAKE_CURRENT_BINARY_DIR}/test)
