"""FelixChecker

FelixChecker checks the status of the FELIX cards before configuration.
"""

import time
import logging


class FelixChecker:

    def __init__(self, timeout):
        self.logger = logging.getLogger('root')
        self.timeout = timeout
        self.ref_dev = 4
        self.ref_fw = 'GBT'

    def read_flx_driver(self):
        t = 0
        while t < self.timeout:
            try:
                f = open("/proc/flx", "r")
                flx_data = f.read()
                f.close()
                check = self.parse(flx_data)
                if check == 1:
                    exit(1)
            except OSError as err:
                self.logger.dbg('/proc/flx not found. %s', err)
                time.sleep(3)
                t += 3
        self.logger.error('Drivers not loaded after 60 seconds')

    def parser(self, data):
        error = 0
        for line in data:
            if "Number of devices" in line:
                found_devices = int(line.split('=')[1].strip(' '))
                if found_devices != self.ref_dev:
                    self.logger.error('Found %d devices, %d expected. Contact read-out on call.', found_devices, self.ref_dev)
                    error = 1
            if "Firmware mode" in line:
                found_fw = line.split(':')[1].strip(' ')
                if found_fw != self.ref_fw:
                    self.logger.error('Found %s firmware, %s expected. Contact read-out on call.', found_fw, self.ref_fw)
                    error = 1
        return error
