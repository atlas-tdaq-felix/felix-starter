"""FelixStarter

Module to communicate with Supervisor's RPC interface
"""

from xmlrpc.client import ServerProxy
from xmlrpc.client import Fault
import logging
from time import sleep


class FelixStarter:

    def __init__(self, server_proxy='http://localhost:9001/RPC2', ers_logging=False):
        """Creates an RPC client and checks that Supervisor is running"""
        self.server = ServerProxy(server_proxy)
        self.sv = self.server.supervisor
        self.logger = logging.getLogger('root')
        self.enable_ers = ers_logging
        if self.enable_ers:
            from .felix_issues import CannotGetInfo, TaskNotStarted, GroupNotStarted, TaskFailed, TaskTimeout
            self.CannotGetInfo = CannotGetInfo
            self.TaskNotStarted = TaskNotStarted
            self.GroupNotStarted = GroupNotStarted
            self.TaskFailed = TaskFailed
            self.TaskTimeout = TaskTimeout
        try:
            self.sv.getState()['statecode']
        except ConnectionRefusedError as err:
            if self.enable_ers:
                from .felix_issues import SupervisorNotStarted
                self.logger.fatal(SupervisorNotStarted(err))
            else:
                self.logger.fatal('Supervisord is not running. %s', err)
            exit(1)
        self.logger.debug('Supervisor RPC client initialised.')

    def get_info(self, task):
        try:
            info = self.sv.getProcessInfo(task)
            return info
        except Fault as err:
            if self.enable_ers:
                logging.warning(self.CannotGetInfo(task, err))
            else:
                logging.warning('Cannot get info about task %s. Error %s', task, err)
            return None

    def start_tasks(self, tasks, wait=False):
        """Starts all processes in ythe list

        Args:
            tasks: group of processes to be started
        """
        for task in tasks:
            try:
                self.logger.info('Starting task %s', task)
                self.sv.startProcess(task, wait)
            except Fault as err:
                tasks.remove(task)
                if err.faultCode == 10:
                    self.logger.debug('Task %s not in config file.', task)
                else:
                    if self.enable_ers:
                        self.logger.error(self.TaskNotStarted(task, err))
                    else:
                        self.logger.error('Cannot start task %s. Error: %s', task, err)

    def start_group(self, group):
        """Starts all processes in the group

        Args:
            group: group of processes to be started
        """
        try:
            self.logger.info('Starting group %s', group)
            self.sv.startProcessGroup(group)
        except Fault as err:
            if err.faultCode == 10:
                self.logger.debug('Group %s not in config file.', group)
                return 1
            else:
                if self.enable_ers:
                    self.logger.error(self.GroupNotStarted(group, err))
                else:
                    self.logger.error('Cannot start group %s. Error: %s', group, err)
                return 1
        return 0

    def synch_start_tasks(self, tasks, timeout=30, step=0.5):
        """Starts tasks described in the config file and block until they succeed.

        Args:
            list: tasks listed as group:name
            timeout (int): timeout
            step (float): time step

        Returns:
            error code (int)
        """
        self.start_tasks(tasks, wait=False)

        t = 0
        error = 0
        while t < timeout:
            for task in tasks:
                info = self.get_info(task)
                if info is None:
                    continue

                state = info['statename']
                exit_status = info['exitstatus']
                self.logger.debug('Task %s state %s ret code %d ', task, state, error)
                if state == 'EXITED' and exit_status == 0:
                    self.logger.info('Task %s completed successfully.', task)
                    tasks.remove(task)
                    if not tasks:
                        self.logger.info('Task set completed. Return code %d', error)
                        return error
                elif state == 'FATAL' or (state == 'EXITED' and exit_status != 0):
                    if self.enable_ers:
                        self.logger.error(self.TaskFailed(task, exit_status))
                    else:
                        self.logger.error('Task %s failed, return code %d', task, exit_status)
                    tasks.remove(task)
                    error = 1
                    if not tasks:
                        self.logger.info('Task set completed. Return code %d', error)
                        return error
                else:
                    sleep(step)
                    t += step
        for task in tasks:
            if self.enable_ers:
                self.logger.error(self.TaskTimeout(task))
            else:
                self.logger.error('Timeout for task %s', task)
        error = 1
        return error
