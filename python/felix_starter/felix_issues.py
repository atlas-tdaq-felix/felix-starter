"""FelixIssues

FelixIssues defines ers issues for felix-starter
"""

import ers


class ErsException(ers.Issue):
    def __init__(self, msg, args, cause):
        ers.Issue.__init__(self, msg, args, cause)


class TaskNotStarted(ErsException):
    def __init__(self, task, error, cause=None):
        ErsException.__init__(self, "Cannot start task %s. Error: %s." % (task, error), {"Task": task, "Error": error}, cause)


class SupervisorNotStarted(ErsException):
    def __init__(self, error, cause=None):
        ErsException.__init__(self, "Supervisord is not running. Error: %s." % error, {"Error": error}, cause)


class CannotGetInfo(ErsException):
    def __init__(self, task, error, cause=None):
        ErsException.__init__(self, "Cannot get info for process %s. Error: %s'" % (task, error), {"Task": task, "Error": error}, cause)


class AuthError(ErsException):
    def __init__(self, cause=None):
        ErsException.__init__(self, "Cannot retrieve RPC interface username and password from config file", cause)


class GroupNotStarted(ErsException):
    def __init__(self, group, error, cause=None):
        ErsException.__init__(self, "Cannot start group %s. Error: %s'" % (group, error), {"Group": group, "Error": error}, cause)


class TaskFailed(ErsException):
    def __init__(self, task, code, cause=None):
        ErsException.__init__(self, "Task %s failed: return code %d." % (task, code), {"Task": task, "Code": code}, cause)


class TaskTimeout(ErsException):
    def __init__(self, task, cause=None):
        ErsException.__init__(self, "Timeout for task %s." % task, {"Task": task}, cause)


class TaskExited(ErsException):
    def __init__(self, task, cause=None):
        ErsException.__init__(self, "Task %s exited unexpectedly." % task, {"Task": task}, cause)


class TaskFatal(ErsException):
    def __init__(self, task, cause=None):
        ErsException.__init__(self, "Start of process %s failed too many times. It will not be restarted." % task, {"Task": task}, cause)


class DriverNotLoaded(ErsException):
    def __init__(self, cause=None):
        ErsException.__init__(self, "Driver not loaded within 60 second.", cause)
